import com.coinepic.common.FiatConverter
import com.coinepic.common.nextDoubleBetween
import com.coinepic.market.AbstractMarketApi
import com.coinepic.market.MarketApi
import com.coinepic.market.bean.*
import com.coinepic.priceindex.CoinDesk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.util.*

class CoinepicApiTest {
    private fun appAccount(): AppAccount {
        return AppAccount(id = 171,
//                accessKey = "a1fecf84-43b4-44a9-925d-6dccec18f340", secretKey = "ec12190d-a339-4605-b6de-fe287221cdb1"
                accessKey = "644015b3fdb5f87800999851e441b3fb805a99a630f71ed182",
                secretKey = "4e503b31b8e4e808f21260357179cdd4964d77ca324cbbe0dd"
        )
    }

        @Test
    @Throws(Exception::class)
    fun testBuyAndCancel() {

        val amount = BigDecimal.valueOf(Random().nextDoubleBetween())
        val price = CoinDesk().getInfoWithDeviation().bpi.eur.rateFloat // usd
        val market = MarketApi.coinepic
        val orderId = market.buy(appAccount(), amount, price, SymbolPair(Symbol.BTC, Symbol.USD))
        var order = market.getOrder(appAccount(), orderId)
        assertNotNull(order)

        assertEquals(OrderStatus.NONE, order.status)
        assertEquals(amount, order.orderAmount)
//        assertEquals(BigDecimal.ZERO, order.processedAmount)
        market.cancel(appAccount(), orderId)
        order = market.getOrder(appAccount(), orderId)
        assertNotNull(order)
        assertEquals(OrderStatus.CANCELLED, order.status)
    }

    @Test
    @Throws(Exception::class)
    fun testSellAndCancel() {

        val amount = BigDecimal.valueOf(Random().nextDoubleBetween())
        val price = CoinDesk().getInfoWithDeviation().bpi.eur.rateFloat // usd
        val market = MarketApi.coinepic
        val orderId = market.sell(appAccount(), amount, price, SymbolPair(Symbol.BTC, Symbol.USD))
        var order = market.getOrder(appAccount(), orderId)
        assertNotNull(order)
        assertEquals(OrderStatus.NONE, order.status)
        assertEquals(amount, order.orderAmount)
//        assertEquals(BigDecimal.ZERO, order.processedAmount)
        market.cancel(appAccount(), orderId)
        order = market.getOrder(appAccount(), orderId)
        assertNotNull(order)
        assertEquals(OrderStatus.CANCELLED, order.status)
    }

    @Test
    @Throws(Exception::class)
    fun testGetInfo() {
        val market = MarketApi.coinepic
        val asset = market.getInfo(appAccount())
        assertNotNull(asset)
    }

//    @Test
//    @Throws(Exception::class)
//    fun testGetOrder() {
//
//        val market = MarketApi.coinepic
//        val orderId = 434669L
//        val order = market.getOrder(appAccount(), orderId)
//        assertNotNull(order)
//    }

    @Test
    @Throws(Exception::class)
    fun testGetRunningOrder() {

        val market = MarketApi.coinepic
        val bitOrders = market.getRunningOrders(appAccount())
        assertTrue(bitOrders.isNotEmpty())
    }


//    @Test
//    @Throws(Exception::class)
//    fun testGetKlineDate() {
//        val market = MarketApi.coinepic
//        val klines = market.getKlineDate(Symbol.BTC)
//
//        val usdKlines = klines.map { convertToUsd(market, it) }
//        assertTrue(usdKlines.isNotEmpty())
//
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun testGetKline5Min() {
//        val market = MarketApi.coinepic
//        val klines = market.getKline5Min(Symbol.BTC)
//        for (kline in klines) {
//            convertToUsd(market, kline)
//        }
//        assertTrue(klines.isNotEmpty())
//
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun testGetKline1Min() {
//        val market = MarketApi.coinepic
//        val klines = market.getKline1Min(Symbol.BTC)
//        for (kline in klines) {
//            convertToUsd(market, kline)
//        }
//        assertTrue(klines.size > 0)
//    }

    @Test
    @Throws(Exception::class)
    fun testTicker() {

        val abstractMarketApi = MarketApi.coinepic
        val ticker = abstractMarketApi.ticker(SymbolPair(Symbol.BTC, Symbol.EUR))
        assertTrue(ticker > BigDecimal.valueOf(0.0))

    }

    @Test
    @Throws(Exception::class)
    fun testDepth() {
        val market = MarketApi.coinepic
        val depth = market.getDepth(SymbolPair(Symbol.BTC, Symbol.EUR), true)
        assertTrue(depth.containsKey("asks"))
        assertTrue(depth.containsKey("bids"))
    }


    private fun convertToUsd(market: AbstractMarketApi, kline: Kline): Kline {
        if (!market.market.isUsd) {
            return Kline(
                    open = FiatConverter.toUsd(kline.open, kline.datetime),
                    high = FiatConverter.toUsd(kline.high, kline.datetime),
                    low = FiatConverter.toUsd(kline.low, kline.datetime),
                    close = FiatConverter.toUsd(kline.close, kline.datetime),
                    vwap = FiatConverter.toUsd(kline.vwap, kline.datetime)
            )
        }
        return kline
    }
}