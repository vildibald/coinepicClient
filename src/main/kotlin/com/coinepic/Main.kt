@file:JvmName("Main")

package com.coinepic

import com.coinepic.market.MarketApi
import com.coinepic.scheduled.BuySellTask
import com.coinepic.scheduled.BuyTask
import com.coinepic.scheduled.EmuAccounts
import java.util.*


const val MIN_AMOUNT_DEVIATION = 0.0
const val MAX_AMOUNT_DEVIATION = 0.01

const val MIN_RANDOM_BTC = 0.05
const val MAX_RANDOM_BTC = 0.8

private const val DELAY_MILLIS = 5000L
private const val PERIOD_MILLIS = 30000L

val BUY_RATE = 0.9.toBigDecimal()

fun main(args: Array<String>) {
    val timer = Timer()
    EmuAccounts.forEachIndexed { i, emuAccount ->
        timer.schedule(BuyTask(emuAccount, MarketApi.coinepic), i * DELAY_MILLIS, PERIOD_MILLIS)
    }
}