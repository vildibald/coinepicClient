package com.coinepic.market.bean
import java.math.BigDecimal

data class Asset(
        val id: Long = 0,
        val appAccountId: Long = 0,
        val market: Market = Market.COINEPIC,
        val availableBtc: BigDecimal = BigDecimal.ZERO,
        val frozenBtc: BigDecimal = BigDecimal.ZERO,
        val availableLtc: BigDecimal = BigDecimal.ZERO,
        val frozenLtc: BigDecimal = BigDecimal.ZERO,
        val availableUsd: BigDecimal = BigDecimal.ZERO,
        val frozenUsd: BigDecimal = BigDecimal.ZERO,
        val availableEur: BigDecimal = BigDecimal.ZERO,
        val frozenEur: BigDecimal = BigDecimal.ZERO
) {

    override fun toString(): String {
        return "Asset{" +
                "id=" + id +
                ", appAccountId=" + appAccountId +
                ", market=" + market +
                ", availableBtc=" + availableBtc +
                ", frozenBtc=" + frozenBtc +
                ", availableLtc=" + availableLtc +
                ", frozenLtc=" + frozenLtc +
                ", availableUsd=" + availableUsd +
                ", frozenUsd=" + frozenUsd +
                ", availableEur=" + availableEur +
                ", frozenEur=" + frozenEur +
                '}'.toString()
    }
}