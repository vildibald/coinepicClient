package com.coinepic.market.bean

enum class OrderType {
    Market {
        override val isMargin: Boolean
            get() = false

        override val isMarket: Boolean
            get() = true

        override val isLimit: Boolean
            get() = false
    },
    Limit {
        override val isMargin: Boolean
            get() = false

        override val isMarket: Boolean
            get() = false

        override val isLimit: Boolean
            get() = true
    },
    Stop {
        override val isMargin: Boolean
            get() = false

        override val isMarket: Boolean
            get() = false

        override val isLimit: Boolean
            get() = false
    },
    TrailingStop {
        override val isMargin: Boolean
            get() = false

        override val isMarket: Boolean
            get() = false

        override val isLimit: Boolean
            get() = false
    },
    MarginLimit {
        override val isMargin: Boolean
            get() = true

        override val isMarket: Boolean
            get() = false

        override val isLimit: Boolean
            get() = true
    },
    MarginMarket {
        override val isMargin: Boolean
            get() = true

        override val isMarket: Boolean
            get() = true

        override val isLimit: Boolean
            get() = false
    };

    abstract val isMargin: Boolean
    abstract val isMarket: Boolean
    abstract val isLimit: Boolean

}
