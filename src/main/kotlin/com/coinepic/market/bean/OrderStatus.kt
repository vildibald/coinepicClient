package com.coinepic.market.bean

import java.util.ArrayList

enum class OrderStatus {
    NONE {

        override val isRunning: Boolean
            get() = true

        override val isCancelled: Boolean
            get() = false

        override val isComplete: Boolean
            get() = false

        override val isNotFinish: Boolean
            get() = true

        override fun toShort(): String {
            return "n"
        }
    },
    PART {


        override val isRunning: Boolean
            get() = true

        override val isCancelled: Boolean
            get() = false

        override val isComplete: Boolean
            get() = false

        override val isNotFinish: Boolean
            get() = true

        override fun toShort(): String {
            return "p"
        }
    },
    COMPLETE {

        override val isRunning: Boolean
            get() = false

        override val isCancelled: Boolean
            get() = false

        override val isComplete: Boolean
            get() = true

        override val isNotFinish: Boolean
            get() = false

        override fun toShort(): String {
            return ""
        }
    },
    CANCELLED {

        override val isRunning: Boolean
            get() = false

        override val isCancelled: Boolean
            get() = true

        override val isComplete: Boolean
            get() = true

        override val isNotFinish: Boolean
            get() = false

        override fun toShort(): String {
            return super.name
        }
    },
    CREATED {

        override val isRunning: Boolean
            get() = false

        override val isCancelled: Boolean
            get() = false

        override val isComplete: Boolean
            get() = false

        override val isNotFinish: Boolean
            get() = true

        override fun toShort(): String {
            return "c"
        }
    };

    abstract val isRunning: Boolean

    abstract val isCancelled: Boolean

    abstract val isComplete: Boolean

    abstract val isNotFinish: Boolean

    abstract fun toShort(): String

    companion object {

        fun notComplete(): Collection<OrderStatus> {
            val collection = ArrayList<OrderStatus>()
            collection.add(CREATED)
            collection.add(NONE)
            collection.add(PART)
            return collection
        }

        fun all(): Collection<OrderStatus> {
            val collection = ArrayList<OrderStatus>()
            collection.add(CREATED)
            collection.add(NONE)
            collection.add(PART)
            collection.add(COMPLETE)
            collection.add(CANCELLED)

            return collection
        }


        fun running(): Collection<OrderStatus> {
            val collection = ArrayList<OrderStatus>()
            collection.add(NONE)
            collection.add(PART)
            return collection
        }

        fun complete(): Collection<OrderStatus> {
            val collection = ArrayList<OrderStatus>()
            collection.add(COMPLETE)
            collection.add(CANCELLED)
            return collection
        }
    }
}
