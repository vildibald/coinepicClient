package com.coinepic.market.bean

import java.time.LocalDateTime
import java.math.BigDecimal

data class Kline(
        val datetime: LocalDateTime = LocalDateTime.now(),
        val market: Market = Market.COINEPIC,
        val timestamp: Long = 0,
        val open: BigDecimal = BigDecimal.ZERO,
        val high: BigDecimal = BigDecimal.ZERO,
        val low: BigDecimal = BigDecimal.ZERO,
        val close: BigDecimal = BigDecimal.ZERO,
        val volume: BigDecimal = BigDecimal.ZERO,
        val symbol: Symbol = Symbol.BTC,
        val vwap: BigDecimal = BigDecimal.ZERO
) {

    fun avgPrice(): BigDecimal {
        return (open + close + high + low) / BigDecimal.valueOf(4)

    }
}