package com.coinepic.market.bean

import java.util.*


enum class Market {

    COINEPIC {
        override val isUsd: Boolean
            get() = false

        override val timeZone: TimeZone
            get() = TimeZone.getTimeZone("GMT+0")
    };

    abstract val isUsd: Boolean

    abstract val timeZone: TimeZone

}

