package com.coinepic.market.bean

enum class Currency {
    EUR, USD
}