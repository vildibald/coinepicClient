package com.coinepic.market.bean

import java.time.LocalDateTime
import java.math.BigDecimal

data class BitOrder(
        val id: Long = 0L,
        val market: String = "",
        val orderId: Long = 0L,
        val orderPrice: BigDecimal = BigDecimal.ZERO,
        val appAccountId: Long = 0L,
        val orderEurPrice: BigDecimal = BigDecimal.ZERO,
        val orderAmount: BigDecimal = BigDecimal.ZERO,
        val timestamp: Long = 0L,
        val datetime: LocalDateTime = LocalDateTime.now(),
        val createTime: LocalDateTime = LocalDateTime.now(),
        val fee: BigDecimal = BigDecimal.ZERO,
        val processedPrice: BigDecimal = BigDecimal.ZERO,
        val processedCnyPrice: BigDecimal = BigDecimal.ZERO,
        val processedAmount: BigDecimal = BigDecimal.ZERO,
        val orderSide: OrderSide,
        val status: OrderStatus = OrderStatus.NONE,
        val info: String = "",
        val orderType: OrderType = OrderType.Limit,
        val timeoutTime: Long = 0L,
        val enable: Boolean = true,
        val symbol: Symbol = Symbol.BTC
){

    override fun toString(): String {
        return "BitOrder{" +
                "id=" + id +
                ", market='" + market + '\''.toString() +
                ", orderId=" + orderId +
                ", orderPrice=" + orderPrice +
                ", appAccountId=" + appAccountId +
                ", orderEurPrice=" + orderEurPrice +
                ", orderAmount=" + orderAmount +
                ", timestamp=" + timestamp +
                ", datetime=" + datetime +
                ", createTime=" + createTime +
                ", timeoutTime=" + timeoutTime +
                ", fee=" + fee +
                ", processedPrice=" + processedPrice +
                ", processedCnyPrice=" + processedCnyPrice +
                ", processedAmount=" + processedAmount +
                ", orderSide=" + orderSide +
                ", status=" + status +
                ", info='" + info + '\''.toString() +
                ", orderType=" + orderType +
                ", enable=" + enable +
                '}'.toString()
    }
}