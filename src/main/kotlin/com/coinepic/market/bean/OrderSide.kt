package com.coinepic.market.bean

enum class OrderSide {
    EMPTY, SELL, BUY
}
