package com.coinepic.market.bean

enum class Symbol {
    BTC {
        override val isBtc: Boolean
            get() = true

        override val isLtc: Boolean
            get() = false

        override val isUsd: Boolean
            get() = false

        override val isEur: Boolean
            get() = false
    },
    LTC {
        override val isBtc: Boolean
            get() = false

        override val isLtc: Boolean
            get() = true

        override val isUsd: Boolean
            get() = false

        override val isEur: Boolean
            get() = false
    },
    USD {

        override val isBtc: Boolean
            get() = false

        override val isLtc: Boolean
            get() = false

        override val isUsd: Boolean
            get() = true

        override val isEur: Boolean
            get() = false
    },
    EUR {
        override val isBtc: Boolean
            get() = false

        override val isLtc: Boolean
            get() = false

        override val isUsd: Boolean
            get() = false

        override val isEur: Boolean
            get() = true
    };

    abstract val isBtc: Boolean

    abstract val isLtc: Boolean

    abstract val isUsd: Boolean

    abstract val isEur: Boolean
}

