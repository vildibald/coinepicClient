package com.coinepic.market.bean

class SymbolPair(val first: Symbol, val second: Symbol) {

    fun getDesc(haveUnderline: Boolean = false): String {
        return if (haveUnderline) {
            first.name.toLowerCase() + "_" + second.name.toLowerCase()
        } else {
            first.name.toLowerCase() + second.name.toLowerCase()

        }
    }
}
