package com.coinepic.market.bean

import java.time.LocalDateTime


@SuppressWarnings("serial")
data class AppAccount(
        val id: Long = 0L,
        val market: Market = Market.COINEPIC,
        val email: String = "",
        val accessKey: String = "",
        val secretKey: String = "",
        val clientId: String = "",
        val enable: Boolean = true,
        val createDate: LocalDateTime = LocalDateTime.now()
)

