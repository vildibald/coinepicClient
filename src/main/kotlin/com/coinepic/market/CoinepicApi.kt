package com.coinepic.market

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.coinepic.common.FiatConverter
import com.coinepic.common.HttpUtils
import com.coinepic.market.bean.*
import com.coinepic.market.bean.Currency
import com.coinepic.market.utils.MarketErrorCode
import com.coinepic.market.utils.TradeException
import com.google.common.base.Throwables
import mu.KotlinLogging
import org.jsoup.nodes.Document
import java.io.IOException
import java.math.BigDecimal
import java.math.BigInteger
import java.nio.charset.Charset
import java.text.ParseException
import java.time.Instant
import java.time.ZoneId

import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

private const val COINEPIC_URL = "https://coinepic.com"
private const val DURATION: Long = 1000
private const val TIME_OUT = 15000

private val LOG = KotlinLogging.logger {  }

class CoinepicApi : AbstractMarketApi(Currency.EUR, Market.COINEPIC) {
    override val transactionFee: BigDecimal
        get() = BigDecimal.ZERO
    override val withdrawalFee: BigDecimal
        get() = BigDecimal.ZERO
    override val depositFee: BigDecimal
        get() = BigDecimal.ZERO

    override fun buy(appAccount: AppAccount, amount: BigDecimal, price: BigDecimal, symbolPair: SymbolPair, orderType: OrderType): Long {
        val params = TreeMap<String, String>()
        params["side"] = "buy"
        val response = trade(appAccount, amount, price, params, symbolPair, orderType)
        return if (response.containsKey("id")) {
            response.getLong("id")
        } else -1L
    }

    override fun sell(appAccount: AppAccount, amount: BigDecimal, price: BigDecimal, symbolPair: SymbolPair, orderType: OrderType): Long {
        val params = TreeMap<String, String>()
        params["side"] = "sell"
        val response = trade(appAccount, amount, price, params, symbolPair, orderType)
        return if (response.containsKey("id")) {
            response.getLong("id")
        } else -1L
    }

    private fun trade(appAccount: AppAccount, amount: BigDecimal, price: BigDecimal, params: TreeMap<String, String>, symbolPair: SymbolPair, orderType: OrderType): JSONObject {
        if (!appAccount.enable) {
            LOG.info("appAccount is disabled {}", appAccount)
            return JSONObject()
        }

        if (orderType.isMargin) {
            throw UnsupportedOperationException()
        }

        params["volume"] = amount.toString()
        params["price"] = price.toString()
        params["market"] = getSymbolPairDescFromUsd2Eur(symbolPair)
        params["canonical_verb"] = "POST"
        params["canonical_uri"] = "/api/v2/orders"
        val response = sendRequest(appAccount, params, TIME_OUT, false)
        if (response.containsKey("error")) {
            throw TradeException(MarketErrorCode.getForCoinepicEUR(response))
        }
        return response
    }

    override fun cancel(appAccount: AppAccount, orderId: Long) {
        val params = TreeMap<String, String>()
        params["id"] = orderId.toString()
        params["canonical_verb"] = "POST"
        params["canonical_uri"] = "/api/v2/order/delete"

        val response = sendRequest(appAccount, params, TIME_OUT, true)
        if (response.containsKey("error")) {
            throw TradeException(MarketErrorCode.getForCoinepicEUR(response))
        }
    }

    private fun getSymbolPairDescFromUsd2Eur(symbolPair: SymbolPair): String {
        if (symbolPair.second.isUsd) {
            val symbolPair1 = SymbolPair(symbolPair.first, Symbol.EUR)
            return symbolPair1.getDesc(false)
        } else if (symbolPair.second.isEur) {
            return symbolPair.getDesc(false)
        }
        throw RuntimeException("symbolPair not contain usd " + symbolPair.getDesc(false))
    }

    override fun updateDepth(symbolPair: SymbolPair): JSONObject {
        val data = this.getJson(symbolPair)
        if (data.containsKey("asks")) {
            val jsonObject = this.formatDepth(data)
            convertToUsd(jsonObject)
            return jsonObject
        }
        throw RuntimeException("update_depth error")
    }

    private fun getJson(symbolPair: SymbolPair): JSONObject {
        val url = COINEPIC_URL + "/api/v2/order_book?market=" + getSymbolPairDescFromUsd2Eur(symbolPair) +
                "&asks_limit=100&bids_limit=100"

        var data = JSONObject()
        try {
            val jsonstr = this.parseJsonStr(url)
            data = JSON.parseObject(jsonstr)
        } catch (e: Exception) {
            LOG.info("{} - Can't parse json message:{}", market, e.message)

            sleep(3000)
            try {
                val jsonstr = this.parseJsonStr(url)
                data = JSON.parseObject(jsonstr)
            } catch (e2: Exception) {
                LOG.error("{} - Can't parse json message:{}", market, e2.message)
            }

        }

        return data
    }

    override fun getInfo(appAccount: AppAccount): Asset {
        val params = TreeMap<String, String>()
        var response: JSONObject?
        try {
            params["canonical_verb"] = "GET"
            params["canonical_uri"] = "/api/v2/members/me"

            response = sendRequest(appAccount, params, TIME_OUT, true)
        } catch (e: Exception) {
            params["canonical_verb"] = "GET"
            params["canonical_uri"] = "/api/v2/members/me"
            response = sendRequest(appAccount, params, TIME_OUT, true)
        }

        if (response == null) {
            throw RuntimeException("Can't get_info")
        }

        var asset = Asset(appAccountId = appAccount.id, market = market)

        val accounts = response.getJSONArray("accounts")
        for (i in accounts.indices) {
            val balance = accounts.getJSONObject(i)
            val currency1 = balance.getString("currency")
            if (currency1 == "btc") {
                asset = asset.copy(availableBtc = balance.getBigDecimal("balance"),
                        frozenBtc = balance.getBigDecimal("locked"))
            }
            if (currency1 == "eur") {
                asset = asset.copy(availableEur = balance.getBigDecimal("balance"),
                        availableUsd = FiatConverter.toUsd(asset.availableEur),
                        frozenEur = balance.getBigDecimal("locked"),
                        frozenUsd = FiatConverter.toUsd(asset.frozenEur))
            }
        }
        return asset
    }

    override fun getKlineDate(symbol: Symbol): List<Kline> {
        val url = getKlineUrl(symbol, 1440)
        val klines = getKlines(url, symbol)
//  What this should do?
//        for (kline in klines) {
//            kline.timestamp(kline.getTimestamp())
//        }
        return klines
    }

    private fun getKlineUrl(symbol: Symbol, period: Int): String {
        return ("""$COINEPIC_URL/api/v2/k?market=${getSymbolPairDescFromUsd2Eur(SymbolPair(symbol, Symbol.EUR))}&period=$period&limit=100""")
    }

    @Throws(ParseException::class, IOException::class)
    private fun getKlines(url: String, symbol: Symbol): List<Kline> {
        val text = HttpUtils.getContentForGet(url, TIME_OUT)
        val lines = JSONArray.parseArray(text)
        val klines = ArrayList<Kline>()
        for (i in lines.indices) {
            val kline = getKline(JSONArray.parseArray(lines.getString(i)), symbol)
            klines.add(kline)
        }
        return klines
    }

    @Throws(ParseException::class)
    private fun getKline(params: JSONArray, symbol: Symbol): Kline {

        val timeStamp = params.getLong(0)!!
        val open = params.getBigDecimal(1)
        val high = params.getBigDecimal(2)
        val low = params.getBigDecimal(3)
        val close = params.getBigDecimal(4)
        val avg = (open + close + high + low) / BigDecimal.valueOf(4.0)

        return Kline(market = market,
                timestamp = timeStamp,
                datetime = Instant.ofEpochMilli(timeStamp * 1000L).atZone(ZoneId.systemDefault())
                        .toLocalDateTime(),
                open = open,
                high = high,
                low = low,
                close = close,
                vwap = avg,
                volume = params.getBigDecimal(5),
                symbol = symbol
        )
    }


    override fun getKline5Min(symbol: Symbol): List<Kline> {
        val url = getKlineUrl(symbol, 5)
        return getKlines(url, symbol)
    }

    override fun getKline1Min(symbol: Symbol): List<Kline> {
        val url = getKlineUrl(symbol, 1)
        return getKlines(url, symbol)
    }

    override fun ticker(symbol: SymbolPair): BigDecimal {
        val ticker_url = COINEPIC_URL + "/api/v2/tickers/" + getSymbolPairDescFromUsd2Eur(symbol)
        val text = HttpUtils.getContentForGet(ticker_url, 5000)
        val jsonObject = JSONArray.parseObject(text)
        val ticker = jsonObject.getJSONObject("ticker")
        return FiatConverter.toUsd(ticker.getBigDecimal("last"))
    }

    override fun getOrder(appAccount: AppAccount, orderId: Long): BitOrder {
        val params = TreeMap<String, String>()
        params["canonical_verb"] = "GET"
        params["canonical_uri"] = "/api/v2/order"
        params["id"] = orderId.toString()
        val response = sendRequest(appAccount, params, TIME_OUT, true)
        return getOrder(response)
    }

    override fun getRunningOrders(appAccount: AppAccount): List<BitOrder> {
        val params = TreeMap<String, String>()
        params["canonical_verb"] = "GET"
        params["canonical_uri"] = "/api/v2/orders"
        params["market"] = getSymbolPairDescFromUsd2Eur(SymbolPair(Symbol.BTC, Symbol.EUR))
        params["limit"] = "100"
        val orders = ArrayList<BitOrder>()
        val ordersResponse = sendRequests(appAccount, params, TIME_OUT, true)
        for (anOrdersResponse in ordersResponse) {
            val orderResponse = anOrdersResponse as JSONObject
            orders.add(getOrder(orderResponse))
        }
        return orders
    }

    private fun sendRequests(appAccount: AppAccount, params: TreeMap<String, String>, timeout:
    Int, isThrow: Boolean): JSONArray {
        val body = internalSendRequest(appAccount, params, timeout)
        val response = JSONObject.parseArray(body)
                ?: throw RuntimeException("send_request response is null")
        if (isThrow && body.contains("error")) {
            throw RuntimeException("send_request:$body")
        }
        return response
    }

    private fun getOrder(jsonObject: JSONObject): BitOrder {
        val eurPrice = jsonObject.getBigDecimal("price")
        val orderStatusStr = jsonObject.getString("state")
        var orderStatus = OrderStatus.NONE
        if ("wait" == orderStatusStr) {
            orderStatus = OrderStatus.NONE
        } else if ("done" == orderStatusStr) {
            orderStatus = OrderStatus.COMPLETE
        } else if ("cancel" == orderStatusStr) {
            orderStatus = OrderStatus.CANCELLED
        }

        return BitOrder(
                orderId = jsonObject.getLong("id"),
                orderAmount = jsonObject.getBigDecimal("volume"),
                orderEurPrice = eurPrice,
                orderPrice = FiatConverter.toUsd(eurPrice),
                processedAmount = jsonObject.getBigDecimal("executed_volume"),
                processedCnyPrice = jsonObject.getBigDecimal("avg_price"),
                processedPrice = FiatConverter.toUsd(jsonObject.getBigDecimal("avg_price")),
                fee = transactionFee,
                orderSide = OrderSide.EMPTY,
                status = orderStatus
        )
    }

    private fun getSign(appAccount: AppAccount, parameters: TreeMap<String, String>): String {
        if (parameters.containsKey("signature")) {
            parameters.remove("signature")
        }

        val parameter = StringBuilder()
        for ((key, value) in parameters) {
            if (key == "canonical_verb" || key == "canonical_uri") {
                continue
            }

            parameter.append("&").append(key).append("=").append(value)
        }
        if (parameter.isNotEmpty()) {
            parameter.deleteCharAt(0)
        }
        val canonicalVerb = parameters["canonical_verb"]
        val canonicalUri = parameters["canonical_uri"]

        val signStr = String.format("%s|%s|%s", canonicalVerb, canonicalUri, parameter.toString())
        try {
            val mac = Mac.getInstance("HmacSHA256")
            val keyspec = SecretKeySpec(appAccount.secretKey.toByteArray(Charset.forName("UTF-8")), "HmacSHA256")
            mac.init(keyspec)
            mac.update(signStr.toByteArray(charset("UTF-8")))
            return String.format("%064x", BigInteger(1, mac.doFinal()))
        } catch (e: Exception) {
            throw Throwables.propagate(e)
        }

    }

    private fun internalSendRequest(appAccount: AppAccount, params: TreeMap<String, String>, timeout: Int): String {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastUpdate < DURATION) {
            try {
                Thread.sleep(DURATION - (currentTime - lastUpdate))
            } catch (e: InterruptedException) {
                // ignore
            }

        }
        params["access_key"] = appAccount.accessKey
        params["tonce"] = createNonce().toString()

        params["signature"] = getSign(appAccount, params)

        val canonicalVerb = params["canonical_verb"]
        params.remove("canonical_verb")
        val canonicalUri = params["canonical_uri"]
        params.remove("canonical_uri")
        LOG.info("send_request params:{}", params)
        val doc: Document
        var response: String? = null
        try {
            val url = COINEPIC_URL + canonicalUri
            val connection = HttpUtils.getConnectionForPost(url, params).timeout(timeout).ignoreContentType(true)
            connection.ignoreHttpErrors(true)
            if ("post".equals(canonicalVerb, ignoreCase = true)) {
                doc = connection.post()
            } else {
                doc = connection.get()
            }
            lastUpdate = System.currentTimeMillis()
            response = doc.body().text()

            return response
        } catch (e: Exception) {
            throw Throwables.propagate(e)
        } finally {
            LOG.info("send_request result:{}", response)
        }
    }

    private fun sendRequest(appAccount: AppAccount, params: TreeMap<String, String>, timeout:
    Int, isThrow: Boolean): JSONObject {
        val body = internalSendRequest(appAccount, params, timeout)
        val response = JSON.parseObject(body)
                ?: throw RuntimeException("send_request response is null")
        if (isThrow && response.containsKey("error")) {
            throw RuntimeException("send_request:" + MarketErrorCode.getForCoinepicEUR(response))
        }
        return response
    }

    override fun sortAndFormat(jsonArray: JSONArray, reverse: Boolean): JSONArray {

        if (reverse) {
            Collections.sort(jsonArray, ReversePriceComparator())
        } else {
            Collections.sort(jsonArray, PriceComparator())

        }
        val jsonArray1 = JSONArray()
        for (i in jsonArray.indices) {
            val jsonObject = jsonArray.getJSONObject(i)
            val jsonObject1 = JSONObject()
            jsonObject1["price"] = jsonObject.getBigDecimal("price")
            jsonObject1["amount"] = jsonObject.getBigDecimal("remaining_volume")
            jsonArray1.add(jsonObject1)

        }
        return jsonArray1
    }

    internal inner class PriceComparator : Comparator<Any> {
        override fun compare(member1: Any, member2: Any): Int {
            return (member1 as JSONObject).getBigDecimal("price")!!.compareTo((member2 as JSONObject).getBigDecimal("price")!!)
        }
    }

    internal inner class ReversePriceComparator : Comparator<Any> {
        override fun compare(member1: Any, member2: Any): Int {
            return (member2 as JSONObject).getBigDecimal("price")!!.compareTo((member1 as JSONObject).getBigDecimal("price")!!)
        }
    }


}