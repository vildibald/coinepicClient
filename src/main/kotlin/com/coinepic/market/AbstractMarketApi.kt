package com.coinepic.market

import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.coinepic.common.FiatConverter
import com.coinepic.common.HttpUtils
import com.coinepic.market.bean.*
import com.coinepic.market.bean.Currency
import java.io.IOException
import java.text.ParseException
import java.util.*
import org.slf4j.LoggerFactory
import java.math.BigDecimal

private val LOG = LoggerFactory.getLogger(AbstractMarketApi::class.java)

abstract class AbstractMarketApi(private val currency: Currency = Currency.EUR, val market: Market) {

    internal var lastUpdate = 0L
    internal var depthUpdated = 10L

    abstract val transactionFee: BigDecimal

    abstract val withdrawalFee: BigDecimal

    abstract val depositFee: BigDecimal


    val wholeFee: BigDecimal
        get() = depositFee + withdrawalFee + transactionFee

    init {
        this.initDepth()
    }

    abstract fun buy(appAccount: AppAccount, amount: BigDecimal, price: BigDecimal, symbolPair:
    SymbolPair, orderType: OrderType = OrderType.Limit): Long

    abstract fun sell(appAccount: AppAccount, amount: BigDecimal, price: BigDecimal, symbolPair:
    SymbolPair, orderType: OrderType = OrderType.Limit): Long

    abstract fun cancel(appAccount: AppAccount, orderId: Long)

    @JvmOverloads
    fun replace(appAccount: AppAccount, orderId: Long, amount: BigDecimal, price: BigDecimal, orderSide: OrderSide, symbolPair: SymbolPair, orderType: OrderType = OrderType.Limit): Long {
        cancel(appAccount, orderId)
        if (OrderSide.BUY == orderSide) {
            return buy(appAccount, amount, price, symbolPair, orderType)
        } else if (OrderSide.SELL == orderSide) {
            return sell(appAccount, amount, price, symbolPair, orderType)
        }
        LOG.error("appAccount:{} orderId:{} amount:{} price:{} side:{} pair:{}", appAccount, orderId,
                amount, price, orderSide, symbolPair)
        throw RuntimeException("replace error orderSide error:$orderSide")
    }

    fun getDepth(symbolPair: SymbolPair, force_update: Boolean): JSONObject {
        val depth: JSONObject = this.askUpdateDepth(symbolPair)
        var timediff = System.currentTimeMillis() / 1000 - this.depthUpdated
        if (!force_update && timediff < this.depthUpdated) {
            sleep(((this.depthUpdated - timediff) * 1000).toInt())
        }
        timediff = System.currentTimeMillis() / 1000 - this.depthUpdated
        if (timediff > MARKET_EXPIRATION_TIME) {
            LOG.warn("Market: {} order book is expired", this.market)
            return this.initDepth()

        }
        return depth
    }

    private fun initDepth(): JSONObject {
        val depthStr = "{'asks': [{'price': 0, 'amount': 0}], 'bids': [{'price': 0, 'amount': 0}]}"
        return JSONArray.parseObject(depthStr)
    }

    private fun askUpdateDepth(symbolPair: SymbolPair): JSONObject {
        try {
            val depth = this.updateDepth(symbolPair)
            this.fixDepth(depth)
            this.depthUpdated = System.currentTimeMillis() / 1000
            return depth
        } catch (e: Exception) {
            LOG.error("Can't update market:" + this.market, e.message)
        }
        return JSONObject()
    }

    private fun fixDepth(depth: JSONObject) {
        val asks = depth.getJSONArray("asks")
        val bids = depth.getJSONArray("bids")

        if (asks.size > MAX_DEPTH_LEN) {
            for (i in asks.size - 1 downTo MAX_DEPTH_LEN) {
                asks.removeAt(i)
            }
        }
        if (bids.size > MAX_DEPTH_LEN) {
            for (i in bids.size - 1 downTo MAX_DEPTH_LEN) {
                bids.removeAt(i)
            }
        }

        if (asks.size < 2 || bids.size < 2) {
            return
        }
        val ask1 = asks.getJSONObject(0).getBigDecimal("price")!!
        val ask2 = asks.getJSONObject(1).getBigDecimal("price")!! * BigDecimal.valueOf(1.0005)
        val removeBids = ArrayList<JSONObject>()
        for (i in 0 until bids.size - 2) {
            val order = bids.getJSONObject(i)
            val price = order.getBigDecimal("price")!!
            if (price > ask1 && price > ask2) {
                removeBids.add(order)
                LOG.warn("Market: {} order has abnormal bids data:{}, aks2:{}", this.market, order, ask2)
            } else {
                break
            }
        }
        bids.removeAll(removeBids)

        if (bids.size < 2) {
            return
        }
        val bid1 = bids.getJSONObject(0).getBigDecimal("price")!!
        val bid2 = bids.getJSONObject(1).getBigDecimal("price")!! * BigDecimal.valueOf(0.9995)
        val removeAsks = ArrayList<JSONObject>()
        for (i in 0 until asks.size - 2) {
            val order = asks.getJSONObject(i)
            val price = order.getBigDecimal("price")!!
            if (price < bid1 && price < bid2) {
                removeAsks.add(order)
                LOG.warn("Market: {} order has abnormal asks data:{}, aks2:{}", this.market, order, bid2)
            } else {
                break
            }
        }
        asks.removeAll(removeAsks)
    }

    protected fun convertToUsd(depth: JSONObject) {

        if (this.currency === Currency.USD) {
            return
        }
        val directions = arrayOf("asks", "bids")
        for (direction in directions) {
            val orders = depth.getJSONArray(direction)
            for (anOrdersResponse in orders) {
                val order = anOrdersResponse as JSONObject
                order["price"] = FiatConverter.toUsd(order.getBigDecimal("price"))
            }
        }
    }

    abstract fun updateDepth(symbol: SymbolPair): JSONObject

    abstract fun getInfo(appAccount: AppAccount): Asset

    @Throws(IOException::class, ParseException::class)
    abstract fun getKlineDate(symbol: Symbol): List<Kline>

    @Throws(IOException::class, ParseException::class)
    abstract fun getKline5Min(symbol: Symbol): List<Kline>

    @Throws(IOException::class, ParseException::class)
    abstract fun getKline1Min(symbol: Symbol): List<Kline>

    @Throws(IOException::class)
    abstract fun ticker(symbol: SymbolPair): BigDecimal

    abstract fun getOrder(appAccount: AppAccount, orderId: Long): BitOrder

    abstract fun getRunningOrders(appAccount: AppAccount): List<BitOrder>

    internal open fun createNonce() = System.currentTimeMillis() /// 1000

    protected fun sleep(time: Int) = Thread.sleep(time.toLong())

    protected fun formatDepth(data: JSONObject): JSONObject {
        val bids = this.sortAndFormat(data.getJSONArray("bids"), true)
        val asks = this.sortAndFormat(data.getJSONArray("asks"), false)
        val jsonObject = JSONObject()
        jsonObject["asks"] = asks
        jsonObject["bids"] = bids
        return jsonObject
    }

    @Throws(IOException::class)
    fun parseJsonStr(url: String): String {
        val objectDoc = HttpUtils.getConnectionForGet(url).ignoreContentType(true).timeout(15000).get()
        return objectDoc.body().text()
    }

    protected open fun sortAndFormat(jsonArray: JSONArray, reverse: Boolean): JSONArray {

        if (reverse) {
            Collections.sort(jsonArray, ReversePriceComparator())
        } else {
            Collections.sort(jsonArray, PriceComparator())

        }
        val jsonArray1 = JSONArray()
        for (i in jsonArray.indices) {
            val jsonArray2 = jsonArray.getJSONArray(i)
            val jsonObject1 = JSONObject()
            jsonObject1["price"] = BigDecimal(jsonArray2[0].toString())
            jsonObject1["amount"] = BigDecimal(jsonArray2[1].toString())
            jsonArray1.add(jsonObject1)

        }
        return jsonArray1
    }

    internal inner class PriceComparator : Comparator<Any> {
        override fun compare(member1: Any, member2: Any): Int {
            return BigDecimal((member1 as JSONArray)[0].toString()).compareTo(BigDecimal((member2 as JSONArray)[0].toString()))
        }
    }

    internal inner class ReversePriceComparator : Comparator<Any> {
        override fun compare(member1: Any, member2: Any): Int {
            return BigDecimal((member2 as JSONArray)[0].toString()).compareTo(BigDecimal((member1 as JSONArray)[0].toString()))

        }
    }

    companion object {
        private const val MARKET_EXPIRATION_TIME = 120L
        private const val MAX_DEPTH_LEN = 300
    }
}

