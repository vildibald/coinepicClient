package com.coinepic.market.utils

class TradeException : RuntimeException {
    val code: MarketErrorCode

    constructor(code: MarketErrorCode) : super("error code:$code") {
        this.code = code
    }

    constructor(message: String, code: MarketErrorCode) : super(message) {
        this.code = code
    }

    constructor(message: String, cause: Throwable, code: MarketErrorCode) : super(message, cause) {
        this.code = code
    }

    constructor(cause: Throwable, code: MarketErrorCode) : super(cause) {
        this.code = code
    }

    override fun toString(): String {
        return "TradeException{" +
                "code=" + code +
                '}'.toString()
    }
}