package com.coinepic.market.utils

import com.alibaba.fastjson.JSONObject

enum class MarketErrorCode {
    MANY_TIMES {
        override val msg: String
            get() = "Too many requests"

        override val isRepeat: Boolean
            get() = true
    },
    INVALID_TIME {
        override val msg: String
            get() = "Invalid submission time"

        override val isRepeat: Boolean
            get() = true
    },
    BUY_HIGHER_CURRENT {
        override val msg: String
            get() = "Buying price cannot be higher than current price"

        override val isRepeat: Boolean
            get() = false
    },
    SELL_LOWER_CURRENT {
        override val msg: String
            get() = "卖出价格不能低于现价"

        override val isRepeat: Boolean
            get() = false
    },
    NOT_ENOUGH_BTC {
        override val msg: String
            get() = "Selling price cannot be lower than current price"

        override val isRepeat: Boolean
            get() = false
    },
    NOT_ENOUGH_CASH {
        override val msg: String
            get() = "Not enough cash"

        override val isRepeat: Boolean
            get() = false
    };

    abstract val msg: String

    abstract val isRepeat: Boolean

    companion object {

        fun getForCoinepicEUR(response: JSONObject): MarketErrorCode {
            val error = response.getJSONObject("error")
            val code = error.getInteger("code")!!
            val msg = error.getString("message")
            when (code) {
                10001 -> return MANY_TIMES
                2002 -> return NOT_ENOUGH_BTC
                else -> throw RuntimeException("code:$code message:$msg")
            }

        }
    }


}
