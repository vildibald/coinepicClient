package com.coinepic.market

import com.coinepic.market.bean.Market


private fun initMap(): Pair<Market, AbstractMarketApi> {
    val coinepic = CoinepicApi()
    return coinepic.market to coinepic
}

object MarketApi : Map<Market, AbstractMarketApi> by mapOf(initMap()) {
    val coinepic: AbstractMarketApi get() = this[Market.COINEPIC] ?: CoinepicApi()
}



