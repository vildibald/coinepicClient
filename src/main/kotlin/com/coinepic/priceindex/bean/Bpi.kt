package com.coinepic.priceindex.bean
import com.alibaba.fastjson.annotation.JSONField
data class Bpi(@JSONField(name="EUR")
               val eur: EUR,
               @JSONField(name="GBP")
               val gbp: GBP,
               @JSONField(name="USD")
               val usd: USD)