package com.coinepic.priceindex.bean

import com.alibaba.fastjson.annotation.JSONField
import java.math.BigDecimal

data class USD(@JSONField(name="symbol")
               val symbol: String = "",
               @JSONField(name="rate_float")
               val rateFloat: BigDecimal = BigDecimal.ZERO,
               @JSONField(name="code")
               val code: String = "",
               @JSONField(name="rate")
               val rate: String = "",
               @JSONField(name="description")
               val description: String = "")