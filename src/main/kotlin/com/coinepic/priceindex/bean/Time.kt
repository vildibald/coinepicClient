package com.coinepic.priceindex.bean

import com.alibaba.fastjson.annotation.JSONField

data class Time(@JSONField(name="updateduk")
                val updateduk: String = "",
                @JSONField(name="updatedISO")
                val updatedISO: String = "",
                @JSONField(name="updated")
                val updated: String = "")