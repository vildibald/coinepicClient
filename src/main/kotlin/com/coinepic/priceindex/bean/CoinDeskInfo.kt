package com.coinepic.priceindex.bean

import com.alibaba.fastjson.annotation.JSONField

data class CoinDeskInfo(@JSONField(name="chartName")
                        val chartName: String = "",
                        @JSONField(name="bpi")
                        val bpi: Bpi,
                        @JSONField(name="time")
                        val time: Time,
                        @JSONField(name="disclaimer")
                        val disclaimer: String = "")