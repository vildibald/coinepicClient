package com.coinepic.priceindex

import com.alibaba.fastjson.JSON
import com.coinepic.common.HttpUtils
import com.coinepic.common.RandomDeviation
import com.coinepic.priceindex.bean.CoinDeskInfo
import mu.KotlinLogging

private val LOG = KotlinLogging.logger { }
private val URL = "https://api.coindesk.com/v1/bpi/currentprice.json"

class CoinDesk {

    fun getInfo(): CoinDeskInfo {
        val content = HttpUtils.getContentForGet(URL, 1000)
        LOG.info("CoinDesk API response: $content")
        return JSON.parseObject(content, CoinDeskInfo::class.javaObjectType)
    }

    fun getInfoWithDeviation(): CoinDeskInfo {
        val info = getInfo()
        val deviator = RandomDeviation()
        val bpi = info.bpi
        val eur = bpi.eur
        val usd = bpi.usd
        val gbp = bpi.gbp

        val newEur = bpi.eur.copy(rateFloat = deviator.deviation(eur.rateFloat))
        val newUsd = bpi.usd.copy(rateFloat = deviator.deviation(usd.rateFloat))
        val newGbp = bpi.gbp.copy(rateFloat = deviator.deviation(gbp.rateFloat))
        val newBpi = bpi.copy(eur = newEur, usd = newUsd, gbp = newGbp)
        return info.copy(bpi = newBpi)

    }

}