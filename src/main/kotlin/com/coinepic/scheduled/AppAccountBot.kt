package com.coinepic.scheduled

import com.coinepic.common.Rounder
import com.coinepic.market.bean.AppAccount

data class AppAccountBot(val appAccount: AppAccount, val rounder: Rounder)

infix fun AppAccount.withRounder(rounder: Rounder): AppAccountBot {
    return AppAccountBot(this, rounder)
}


