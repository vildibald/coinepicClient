package com.coinepic.scheduled

import com.coinepic.common.RounderToNearest
import com.coinepic.market.bean.AppAccount

private const val BASE_EMU = 172L

object EmuAccounts : List<AppAccountBot> by listOf(
        AppAccount(id = BASE_EMU + 0,
                accessKey = "mqnefmfijacuicicictdrmhofhzheqibqmsjherrgiwitfbjde",
                secretKey = "pagloujlocwjbdcyyydyvcxdwytdrbaxglaxsvgianqpydiuan")
                withRounder RounderToNearest.FIVE,

        AppAccount(id = BASE_EMU + 1,
                accessKey = "jhducsthxuypfwppjwlzyarbtmrsloycgasxphewdtuzsaamcc",
                secretKey = "etaisvuxevlktdauqaeiqsmsvyborgagofuczprleyrilmfjge")
                withRounder RounderToNearest.HALF,

        AppAccount(id = BASE_EMU + 2,
                accessKey = "kleyqvccawafomitftkngbtvgbnwogippbvqoqfgmvoecdujkf",
                secretKey = "dgkyhdahafsxcyubnbcplxheqtwtjeiczkvulpisfrgvkrdxjn")
                withRounder RounderToNearest.TWO_DECIMALS,

        AppAccount(id = BASE_EMU + 3,
                accessKey = "zuxtvmmjkwcabyihhcbibzibdqlnrvgtlkfnzawgsdjosffcmn",
                secretKey = "sdooozjcghbpvrppkwklldnzqenrcbzvfnxnrqucnabrbwetex")
                withRounder RounderToNearest.ONE_DECIMAL,

        AppAccount(id = BASE_EMU + 4,
                accessKey = "ttoospzdxzsnbylxztqfivftiugyppoojoadwnjyzbmgwjcgus",
                secretKey = "fsypsymngjohmkydxgzugdjtropercggfpcsmkdmzesspumndp")
                withRounder RounderToNearest.INTEGER,

        AppAccount(id = BASE_EMU + 5,
                accessKey = "mozmfqeyucwhdjinnotlnmhgqeacltijshbrvfeifcnmumqfvx",
                secretKey = "hqjbgjhmxlreecknwnaokmthacmzjlndedgbmztkuqhtqsjeod")
                withRounder RounderToNearest.TEN,

        AppAccount(id = BASE_EMU + 6,
                accessKey = "ndagqbijpzkblsmuhzqxczipzgvpxgfhodaxttvbugkoxvwpde",
                secretKey = "bashzudqmojhkqxayrzxvmridcdvkupnirgjesqbohtcyqkabn")
                withRounder RounderToNearest.HUNDRED,

        AppAccount(id = BASE_EMU + 7,
                accessKey = "zijyyntahkeixqugehywrdactdqzjoyljoezdrvgklrmnmvwzi",
                secretKey = "yaknnyspeyntfmsbzkvckxlwvnpikoecqvgmzsficvspfuyllz")
                withRounder RounderToNearest.HALF,

        AppAccount(id = BASE_EMU + 8,
                accessKey = "ozqavltnyhiceaskkggafexhoqgtjowrqfhgqvawnzunbmwhka",
                secretKey = "zmrikspsjojxthswfedzortyzajgnlfncwwdlvztaqfxybfrhz")
                withRounder RounderToNearest.FIVE,

        AppAccount(id = BASE_EMU + 9,
                accessKey = "dggjujsvldkiwhsrhygauglanmnpxlsjindgcsnmzcjdqrlmrq",
                secretKey = "kwnyheniotyytrogurqcluljwgowjicpwovpsotiyoizbsbeto")
                withRounder RounderToNearest.INTEGER
)

