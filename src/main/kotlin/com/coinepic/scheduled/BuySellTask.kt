package com.coinepic.scheduled

import com.coinepic.common.nextDoubleBetween
import com.coinepic.market.AbstractMarketApi
import com.coinepic.market.bean.Symbol
import com.coinepic.market.bean.SymbolPair
import com.coinepic.market.utils.TradeException
import com.coinepic.priceindex.CoinDesk
import java.math.BigDecimal
import java.util.*

class BuySellTask(private val appAccountBot: AppAccountBot, private val marketApi: AbstractMarketApi) : TimerTask() {
    private var buyTime = false

    override fun run() {
        try {
            val amount = BigDecimal.valueOf(Random().nextDoubleBetween())
            val price = CoinDesk().getInfoWithDeviation().bpi.eur.rateFloat // usd
            val roundedPrice = appAccountBot.rounder.round(price)
            val orderId: Long
            if (buyTime) {
                orderId = marketApi.buy(appAccountBot.appAccount, amount, roundedPrice, SymbolPair(Symbol.BTC, Symbol.USD))
                buyTime = false
            } else {
                orderId = marketApi.sell(appAccountBot.appAccount, amount, roundedPrice, SymbolPair(Symbol.BTC, Symbol.USD))
                buyTime = true
            }
        } catch (te: TradeException) {
            buyTime = !buyTime
        } catch (e: Exception) {
            buyTime = !buyTime
        }
//        marketApi.cancel(appAccountBot, orderId)
    }

}