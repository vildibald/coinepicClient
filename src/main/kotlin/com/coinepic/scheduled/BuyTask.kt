package com.coinepic.scheduled

import com.coinepic.BUY_RATE
import com.coinepic.common.nextDoubleBetween
import com.coinepic.market.AbstractMarketApi
import com.coinepic.market.bean.Symbol
import com.coinepic.market.bean.SymbolPair
import com.coinepic.market.utils.TradeException
import com.coinepic.priceindex.CoinDesk
import java.math.BigDecimal
import java.util.*

class BuyTask(private val appAccountBot: AppAccountBot, private val marketApi: AbstractMarketApi) : TimerTask() {
    override fun run() = try {
        val amount = BigDecimal.valueOf(Random().nextDoubleBetween())
        val price = CoinDesk().getInfoWithDeviation().bpi.eur.rateFloat * BUY_RATE// usd
        val roundedPrice = appAccountBot.rounder.round(price)
        val orderId = marketApi.buy(appAccountBot.appAccount, amount, roundedPrice, SymbolPair(Symbol.BTC, Symbol.EUR))
    } catch (te: TradeException) {
    } catch (e: Exception) {
    }
}