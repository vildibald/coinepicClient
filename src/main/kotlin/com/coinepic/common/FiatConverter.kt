package com.coinepic.common

import com.coinepic.priceindex.CoinDesk
import java.math.BigDecimal
import java.time.LocalDateTime


object FiatConverter {

    @JvmOverloads
    fun toUsd(eur: BigDecimal, date: LocalDateTime = LocalDateTime.now()): BigDecimal {
        if (eur == BigDecimal.ZERO) return BigDecimal.ZERO
        val usd = eur / getRate(date)
        return usd
    }

    private fun getRate(date: LocalDateTime = LocalDateTime.now()): BigDecimal {
        val bpi = CoinDesk().getInfo().bpi
        return bpi.usd.rateFloat / bpi.eur.rateFloat
    }


    fun toEur(usd: BigDecimal): BigDecimal {
        if (usd == BigDecimal.ZERO) {
            return BigDecimal.ZERO
        }
        val eur = usd * getRate(LocalDateTime.now())
        return eur
    }
}
