package com.coinepic.common

import com.coinepic.MAX_AMOUNT_DEVIATION
import com.coinepic.MIN_AMOUNT_DEVIATION
import java.math.BigDecimal
import java.util.*

class RandomDeviation() {

    fun deviation(value: Double,
                  minDeviation: Double = MIN_AMOUNT_DEVIATION,
                  maxDeviation: Double = MAX_AMOUNT_DEVIATION)
            : Double {
        val random = Random()
        val deviation = random.nextDoubleBetween(minDeviation, maxDeviation)
        val deviatedValue = value * deviation
        return if (random.nextDouble() < 0.5) value - deviatedValue else value + deviatedValue
    }

    fun deviation(value: BigDecimal,
                  minDeviation: Double = MIN_AMOUNT_DEVIATION,
                  maxDeviation: Double = MAX_AMOUNT_DEVIATION)
            : BigDecimal {
        return BigDecimal.valueOf(deviation(value.toDouble(), minDeviation, maxDeviation))
    }
}