package com.coinepic.common

import com.coinepic.MAX_RANDOM_BTC
import com.coinepic.MIN_RANDOM_BTC
import java.math.BigDecimal
import java.util.*


fun Random.nextDoubleBetween(min: Double = MIN_RANDOM_BTC, max: Double = MAX_RANDOM_BTC): Double {
    return min + (max - min) * this.nextDouble()
}