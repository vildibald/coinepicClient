package com.coinepic.common

import java.math.BigDecimal

interface Rounder {
    fun round(value: Double): Double

    fun round(value: BigDecimal): BigDecimal {
        return round(value.toDouble()).toBigDecimal()
    }

}