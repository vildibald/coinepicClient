package com.coinepic.common

import java.math.BigDecimal

enum class RounderToNearest : Rounder {
    NOTHING() {
        override fun round(value: Double): Double = value

        override fun round(value: BigDecimal) = value
    },
    TWO_DECIMALS() {
        override fun round(value: Double): Double {
            return decimalRound(value, 2)
        }
    },
    ONE_DECIMAL() {
        override fun round(value: Double): Double {
            return decimalRound(value, 1)
        }
    },
    HALF() {
        override fun round(value: Double): Double {
            return Math.round(value * 2) / 2.0
        }
    },
    INTEGER() {
        override fun round(value: Double): Double {
            return digitRound(value, 1)
        }
    },
    FIVE() {
        override fun round(value: Double): Double {
            return Math.round(value / 5) * 5.0
        }
    },
    TEN() {
        override fun round(value: Double): Double {
            return digitRound(value, 2)
        }
    },
    HUNDRED() {
        override fun round(value: Double): Double {
            return digitRound(value, 3)
        }
    };

    private companion object {
        fun decimalRound(value: Double, precision: Int): Double {
            val scale = Math.pow(10.0, precision.toDouble()).toInt()
            return Math.round(value * scale).toDouble() / scale
        }

        fun digitRound(value: Double, precision: Int): Double {
            val scale = Math.pow(10.0, (precision - 1).toDouble()).toInt()
            return Math.round(value / scale).toDouble() * scale
        }
    }
}