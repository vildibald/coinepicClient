package com.coinepic.common

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import java.net.SocketTimeoutException

object HttpUtils {

    var USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0"

    fun getConnectionForPost(url: String, datas: Map<String, String>): Connection {
        var url = url
        url = appendHttpString(url)
        val connection = Jsoup.connect(url)
                .userAgent(USER_AGENT).timeout(5000)
                .method(Connection.Method.POST)
        if (!datas.isEmpty()) {
            connection.data(datas)
        }
        return connection
    }

    private fun appendHttpString(url: String): String {
        var url = url
        if (!url.startsWith("http")) {
            url = "http://$url"
        }
        return url
    }

    fun getConnectionForGetNoCookies(url: String, vararg datas: Map<String, String>): Connection {
        var url = url
        url = appendHttpString(url)
        val connection = Jsoup.connect(url).userAgent(USER_AGENT).ignoreContentType(true).timeout(5000)
        if (datas.isNotEmpty() && !datas[0].isEmpty()) {
            connection.data(datas[0])
        }

        return connection
    }

    fun getConnectionForGet(url: String, vararg datas: Map<String, String>): Connection {
        return getConnectionForGetNoCookies(url, *datas)
    }

    fun getContentForGet(url: String, timeout: Int): String {
        try {
            var objectDoc: Document
            try {
                val connection = getConnectionForGetNoCookies(url).timeout(timeout)
                objectDoc = connection.get()
            } catch (e: SocketTimeoutException) {
                try {
                    Thread.sleep(3000)
                } catch (e1: InterruptedException) {
                    // ignore
                }

                val connection = getConnectionForGetNoCookies(url).timeout(timeout)
                objectDoc = connection.get()
            }

            return objectDoc.body().text()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}
